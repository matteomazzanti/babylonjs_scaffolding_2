import "@babylonjs/core/Debug/debugLayer";
import "@babylonjs/inspector";
import "@babylonjs/loaders/glTF";
import {
  ArcRotateCamera,
  Engine,
  EngineFactory,
  HemisphericLight,
  Mesh,
  MeshBuilder,
  Scene,
  Vector3
} from "@babylonjs/core";

class App {

  private _canvas: HTMLCanvasElement;
  private _scene: Scene;
  private _engine: Engine;


  constructor() {
    this._canvas = this._createCanvas();

    // initialize babylon scene and engine
    this._init();

  }


  private async _init(): Promise<void> {
    this._engine = (await EngineFactory.CreateAsync(this._canvas, undefined)) as Engine;
    this._scene = new Scene(this._engine);

    //**for development: make inspector visible/invisible
    window.addEventListener("keydown", (ev) => {
      //Shift+Ctrl+Alt+I
      if (ev.shiftKey && ev.ctrlKey && ev.altKey && ev.key === 'F') {
        if (this._scene.debugLayer.isVisible()) {
          this._scene.debugLayer.hide();
        } else {
          this._scene.debugLayer.show();
        }
      }
    });


    //MAIN render loop & state machine
    await this._main();
  }

  //set up the canvas
  private _createCanvas(): HTMLCanvasElement {

    //Commented out for development
    document.documentElement.style["overflow"] = "hidden";
    document.documentElement.style.overflow = "hidden";
    document.documentElement.style.width = "100%";
    document.documentElement.style.height = "100%";
    document.documentElement.style.margin = "0";
    document.documentElement.style.padding = "0";
    document.body.style.overflow = "hidden";
    document.body.style.width = "100%";
    document.body.style.height = "100%";
    document.body.style.margin = "0";
    document.body.style.padding = "0";

    //create the canvas html element and attach it to the webpage
    this._canvas = document.createElement("canvas");
    this._canvas.style.width = "100%";
    this._canvas.style.height = "100%";
    this._canvas.id = "gameCanvas";
    document.body.appendChild(this._canvas);

    return this._canvas;
  }


  private async _main(): Promise<void> {

    this._test();
    this._engine.runRenderLoop(() => {
      this._scene.render();
    });
    
    //resize if the screen is resized/rotated
    window.addEventListener('resize', () => {
      this._engine.resize();
    });

    
    
  }


  private _test(): void {
    var camera: ArcRotateCamera = new ArcRotateCamera(
      "Camera", 
      Math.PI / 2, 
      Math.PI / 2, 2,
      Vector3.Zero(), 
      this._scene
    );
    camera.attachControl(this._canvas, true);
    var light1: HemisphericLight = new HemisphericLight(
      "light1", 
      new Vector3(1, 1, 0), 
      this._scene
    );
    var sphere: Mesh = MeshBuilder.CreateSphere(
      "sphere", 
      { diameter: 1 }, 
      this._scene
    );
  }

}
new App();